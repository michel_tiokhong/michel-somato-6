var editables = {
    '#accueil': '#art-accueil',
    '#somato': '#art-somato',
    '#bienfaits': '#art-bienfaits',
    '#parcours': '#art-parcours',
    '#liens': '#art-liens'
};

jQuery(document).ready(function($) {
    Object.keys(editables).forEach(function(key)
    {
        $(editables[key]).raptor({
            plugins: {
                // The save UI plugin/button
                save: {
                    // Specifies the UI to call the saveRest plugin to do the actual saving
                    plugin: 'saveRest'
                },
                saveRest: {
                    // The URI to send the content to
                    url: '/edit/edit.php',
                    // Returns an object containing the data to send to the server
                    data: function(html) {
                        return {
                            id: key,
                            content: html
                        };
                    }
                }
            }
        });
    });
});
